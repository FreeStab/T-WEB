export default function(app) {
  // console.log(localStorage)
  if (!process.server) {
    app.$axios.setHeader('Authorization', `Bearer ${localStorage.getItem('token')}`)
    app.$axios.setToken(localStorage.getItem('token'), 'Bearer')
  }
}
