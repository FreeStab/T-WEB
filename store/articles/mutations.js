export default {
  setArticles(state, articles) {
    state.articles = articles
  },
  toggle(state, todo) {
    todo.done = !todo.done
  }
}
