export default {
  fetchArticles({ commit }) {
    return this.$axios(
      {
        method: 'get',
        url: '/articles/AllArticles'
      }
    )
      .then((res) => {
        commit('setArticles', res.data.articles)
      })
      .catch((error) => {
        console.log(error.statusText)
      })
  }
}
