export default {
  setSnackbarMessage(state, message) {
    state.snackbar.message = message
  },
  setSnackbarColor(state, color) {
    state.snackbar.color = color
  },
  setSnackbarStatus(state, status) {
    state.snackbar.status = status
  }
}
