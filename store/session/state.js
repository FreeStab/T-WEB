export default () => ({
  loading: false,
  snackbar: {
    color: 'success',
    status: false,
    message: ''
  }
})
