export default {
  addNotification({ commit }, snackbar) {
    commit('setSnackbarStatus', true)
    if (snackbar.message) {
      commit('setSnackbarMessage', snackbar.message)
    }
    if (snackbar.color) {
      commit('setSnackbarColor', snackbar.color)
    }
    setTimeout(() => {
      commit('setSnackbarStatus', false)
    }, 3000)
  }
}
