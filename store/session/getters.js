export default {
  snackbar(state) {
    return state.snackbar
  },
  crypto(state) {
    return state.crypto
  }
}
