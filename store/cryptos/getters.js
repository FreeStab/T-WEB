export default {
  cryptos(state) {
    return state.cryptos
  },
  missingCryptos(state) {
    return state.missingCryptos
  },
  crypto(state) {
    return state.crypto
  }
}
