export default {
  fetchAllCryptos({ commit }) {
    return this.$axios({
      method: 'get',
      url: '/cryptos',
      params: {
        id: localStorage.getItem('userId')
      }
    })
      .then((res) => {
        commit('setCryptos', res.data)
      })
      .catch((error) => {
        console.log(error)
      })
  },
  setCryptoVisibility({ dispatch }, { symbol, visible }) {
    return this.$axios({
      method: 'put',
      url: '/cryptos/setVisible',
      params: {
        symbol,
        visible
      }
    })
      .then((res) => {
        dispatch('session/addNotification',
          {
            message: 'Visibility updated with success',
            color: 'success'
          },
          { root: true })
      })
      .catch((error) => {
        dispatch('session/addNotification',
          {
            message: error,
            color: 'error'
          },
          { root: true })
      })
  },
  fetchCryptos({ commit }) {
    return this.$axios({
      method: 'get',
      url: '/cryptos/userCryptos',
      params: {
        id: localStorage.getItem('userId')
      }
    })
      .then((res) => {
        commit('setCryptos', res.data)
      })
      .catch((error) => {
        console.log(error)
      })
  },
  fetchCryptosInfos({ commit }) {
    return this.$axios({
      method: 'get',
      url: `/cryptos/${localStorage.getItem('userId')}/1`,
      params: {
        id: localStorage.getItem('userId')
      }
    })
      .then((res) => {
        commit('setCryptos', res.data)
      })
      .catch((error) => {
        console.log(error)
      })
  },
  fetchMissingCryptos({ commit, dispatch }) {
    return this.$axios({
      method: 'get',
      url: '/cryptos/getListCryptoDispo',
      params: {
        userId: localStorage.getItem('userId')
      }
    })
      .then((res) => {
        commit('setMissingCryptos', res.data)
      })
      .catch((error) => {
        dispatch('session/addNotification',
          {
            message: error,
            color: 'error'
          },
          { root: true })
      })
  },
  fetchCrypto({ commit, dispatch }, params) {
    return this.$axios({
      method: 'get',
      url: '/cryptos/getPeriod',
      params: {
        date: Math.floor(Date.now() / 1000),
        ...params
      }
    })
      .then((res) => {
        const datas = res.data.Data.map((data) => {
          return {
            open: data.open,
            time: data.time
          }
        })
        commit('setCrypto', datas)
      })
      .catch((error) => {
        dispatch('session/addNotification',
          {
            message: error,
            color: 'error'
          },
          { root: true })
        console.log(error)
      })
  },
  addUserCryptos({ commit, dispatch }, symbol) {
    return this.$axios({
      method: 'post',
      url: '/cryptos/addUserCryptos',
      params: {
        symbol,
        userId: localStorage.getItem('userId')
      }
    })
      .then((res) => {
        dispatch('session/addNotification',
          {
            message: 'Crypto Added Successfully',
            color: 'success'
          },
          { root: true })
        dispatch('fetchCryptos')
        dispatch('fetchMissingCryptos')
      })
      .catch((error) => {
        dispatch('session/addNotification',
          {
            message: error,
            color: 'error'
          },
          { root: true })
      })
  }
}
