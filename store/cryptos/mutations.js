export default {
  setCryptos(state, cryptos) {
    state.cryptos = cryptos
  },
  setCrypto(state, crypto) {
    state.crypto = crypto
  },
  setMissingCryptos(state, missingCryptos) {
    state.missingCryptos = missingCryptos
  }
}
