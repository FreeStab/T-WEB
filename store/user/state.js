export default () => ({
  user: {
    username: '',
    email: '',
    isAdmin: false
  },
  token: ''
})
