export default {
  setUser(state, user) {
    state.user = {
      id: user.id,
      username: user.username,
      email: user.email,
      isAdmin: user.isAdmin
    }
  },
  setToken(state, token) {
    state.token = token
  }
}
