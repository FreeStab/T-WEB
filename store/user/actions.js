export default {
  updateUser({ commit }, userInfo) {
    console.log(userInfo)
    localStorage.setItem('userId', userInfo.user.id)
    commit('setUser', userInfo.user)
  },
  updateToken({ commit }, token) {
    localStorage.setItem('token', token)
    commit('setToken', token)
  },
  emptyToken({ commit }) {
    localStorage.removeItem('token')
    commit('setToken', '')
  }
}
