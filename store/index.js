import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export const store = new Vuex.Store({
  namespaced: true,
  state: {
    user: {},
    accessToken: null,
    loggingIn: false,
    loginError: null,
    roles: []
  },
  mutations: {
    loginStart: (state) => { state.loggingIn = true },
    loginStop: (state, errorMessage) => {
      state.loggingIn = false
      state.loginError = errorMessage
    },
    updateAccessToken: (state, accessToken) => {
      state.accessToken = accessToken
    },
    updateUser: (state, user) => {
      sessionStorage.setItem('user', JSON.stringify(user))
      state.user = user
    },
    logOut: (state) => {
      state.accessToken = null
      sessionStorage.clear()
    },
    updateRoles: (state, roles) => {
      state.roles = roles
    }
  },
  getters: {
    getUser: state => state.user,
    getRoles: state => state.roles,
    getToken: state => state.accessToken
  },
  actions: {
    doLogin({ commit }, loginData) {
      commit('loginStart')
      axios.post('/users/login', {
        ...loginData
      })
        .then((response) => {
          localStorage.setItem('accessToken', response.data.token)
          commit('loginStop', null)
          commit('updateAccessToken', response.data.token)
          commit('updateUser', response.data.user)

          this.$router.push('/')
        })
        .catch((error) => {
          commit('loginStop', error.response.data.error)
          commit('updateAccessToken', null)
        })
    },
    fetchAccessToken({ commit }) {
      commit('updateAccessToken', localStorage.getItem('accessToken'))
    },
    fetchUser({ commit }) {
      console.log(JSON.parse(sessionStorage.getItem('user')))
      commit('updateUser', JSON.parse(sessionStorage.getItem('user')))
    },
    logOut({ commit }) {
      localStorage.removeItem('accessToken')
      commit('logOut')
      this.$router.push('/login')
    },
    setRoles({ commit }, roles) {
      commit('updateRoles', roles)
    }
  }
})
